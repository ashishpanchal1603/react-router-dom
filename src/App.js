import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Registration from './components/registration/Registration'
import Login from './components/login/Login'
import Dashboard from './components/dashboard/Dashboard'
import NotFound from './components/pages/NotFound'
import Navbar from './components/pages/Navbar'
import Furniture from './components/furniture/Furniture'
import Blog from './components/furniture/Blog'
import Profile from './components/profile/Profile.jsx'
import Personal from './components/profile/Personal'
import NavbarProfile from './components/profile/NavbarProfile'
import Buy from './components/furniture/Buy'
function App () {
  return (
    <>
    <Router>
      <Routes>
        <Route path='/' element={<Login/>}/>
        <Route path='/registration' element={<Registration/>}/>
        <Route path='/dashboard' element={<Dashboard/>}>
          <Route index element={<Navbar/>}/>
          </Route>
          <Route path='/furniture' element={<Furniture/>}/>
          <Route path='/blog' element={<Blog/>}/>
          <Route path='/buy' element={<Buy/>}/>
          <Route path='/profile' element={<Profile/>}>
            <Route index element={<NavbarProfile/>}/>
          <Route path=':id' element={<Personal/>}/>
          <Route path='New' element={<Registration/>}/>
          </Route>
        <Route path='*' element={<NotFound/>}/>
      </Routes>
    </Router>
    </>
  )
}

export default App
