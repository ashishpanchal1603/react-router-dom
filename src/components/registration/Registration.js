import React, { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import './Registration.css'
const Registration = () => {
  const navigate = useNavigate()
  const [userRegistration, setUserRegistration] = useState({
    name: '',
    email: '',
    age: '',
    password: ''
  })
  const [records, setRecords] = useState([])

  const handleChange = (e) => {
    const { name, value } = e.target
    setUserRegistration({ ...userRegistration, [name]: value })
  }

  const handleSubmit = (e) => {
    setRecords(records => [...records, userRegistration])
    console.log('records', records)
    localStorage.setItem('formValue', JSON.stringify([...records, userRegistration]))
    e.preventDefault()
    navigate('/')
  }

  useEffect(() => {
    const items = JSON.parse(localStorage.getItem('formValue'))
    console.log('getData', items)
    if (items) {
      setRecords(items)
    }
  }, [])
  return (
    <>
     <div className="container-form">
      <div className="title">
        <h1>Registration Form</h1>
      </div>
      <form onSubmit={(e) => handleSubmit(e)}>
        <div className="name">
          <label htmlFor="userName">userName</label>
          <input type="text"name="name"
              id="name"
              onChange={(e) => handleChange(e)}
              required={true} />
        </div>
        <div className="Email">
          <label htmlFor="Email">Email</label>
          <input type="email" name="email"
              onChange={(e) => handleChange(e)}
              required={true} />
        </div>
        <div className="Age">
          <label htmlFor="age">Age</label>
          <input type="text" name="age"
              onChange={(e) => handleChange(e)}
              required={true}/>
        </div>
        <div className="Password">
          <label htmlFor="Password">password</label>
          <input type="password" name="password"
              onChange={(e) => handleChange(e)}
              required={true}/>
        </div>

          <button type="submit" className='btn btn-primary'>Submit</button>
        <div>
          <Link to="/">You Have All ready Account Goto Sing in</Link>
        </div>
      </form>
    </div>

    </>
  )
}
export default Registration
