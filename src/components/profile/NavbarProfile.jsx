import React from 'react'
import { NavLink } from 'react-router-dom'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
function Navbar () {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <NavLink className="navbar-brand">Yudiz Solution</NavLink>
          <div className="collapse navbar-collapse" id="navbarScroll">
            <ul
              className="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll"
            >
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/profile"
                >
                  DashBoard
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/registration">
                  Registration
                </NavLink>
              </li>
            </ul>
            <form className="d-flex">
                <NavLink to="/dashboard" >
              <button className="btn btn-outline-success" type="submit">
                Back
              </button>
                </NavLink>
            </form>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Navbar
