import React from 'react'
import { useOutletContext, useParams } from 'react-router-dom'
function Personal () {
  const { id } = useParams()
  const context = useOutletContext()
  return (
    <>
    <h1>personal  {id}</h1>
    <h2>{context.hello}</h2>
    </>
  )
}

export default Personal
