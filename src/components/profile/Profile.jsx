import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Outlet } from 'react-router-dom'
function Book () {
  const [data, setData] = useState([])
  const dataShow = () => {
    axios.get('https://6364aace7b209ece0f4adfbb.mockapi.io/data').then((res) => setData(res.data))
  }
  useEffect(() => {
    dataShow()
  }, [])
  return (
    <>
    <Outlet context={{ hello: 'Ashish panchal' }}/>
  <h1>UseProfile</h1>
<div>
  <table className='table table-striped text-center'>
    {data.map((item, index) => {
      return (
        <React.Fragment key={index}>

                <tr onClick={(e, index) => console.log(index)}>
                  <td>{index}</td>
                  <td>{item.name}</td>
                </tr>
        </React.Fragment>
      )
    })}
    </table>
</div>
    </>
  )
}

export default Book
