import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import './Login.css'
function Login () {
  const navigate = useNavigate()
  const [userRegistration, setUserRegistration] = useState({
    email: '',
    password: ''
  })

  const handleChange = (e) => {
    const { name, value } = e.target
    setUserRegistration({ ...userRegistration, [name]: value })
  }

  const handleSubmit = (e) => {
    const formValue = JSON.parse(localStorage.getItem('formValue'))

    const findValueTwo = formValue.find((element) =>
      element.email === userRegistration.email
    )
    if (findValueTwo.password === userRegistration.password) {
      sessionStorage.setItem('email', findValueTwo.email)
      sessionStorage.setItem('password', findValueTwo.password)
      navigate('/dashBoard')
    } else {
      alert('email or Password are incorrect')
    }
    e.preventDefault()
  }

  return (
    <>
     <div className="container">
      <div className="title">
        <h1>Registration Form</h1>
      </div>
      <form onSubmit={(e) => handleSubmit(e)}>
      <div className="Email">
          <label htmlFor="Email">Email</label>
          <input type="email" name="email"
              onChange={(e) => handleChange(e)}
              required={true} />
        </div>
        <div className="Password">
          <label htmlFor="Password">password</label>
          <input type="password" name="password"
              onChange={(e) => handleChange(e)}
              required={true}/>
        </div>
        <div className="btn">
          <button type="submit">Submit</button>
        </div>
        <div>
          <Link to="/registration">Go Back To Registration</Link>
        </div>
      </form>
    </div>
    </>
  )
}

export default Login
