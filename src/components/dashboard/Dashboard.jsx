import React from 'react'
import { Outlet } from 'react-router-dom'
function Dashboard () {
  const getEmail = sessionStorage.getItem('email')
  const getPassword = sessionStorage.getItem('password')
  console.log('email', getEmail)
  console.log('getPwd', getPassword)

  return (
    <>
    <Outlet/>
    <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>Email</th>
              <th>Password</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{getEmail}</td>
              <td>{getPassword}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  )
}

export default Dashboard
