import React from 'react'

import './Blog.css'
import { useLocation, useNavigate } from 'react-router-dom'

function Blog () {
  const navigate = useNavigate()
  const { state } = useLocation()
  const data = useLocation()
  const buy = data
  // console.log('data :>> ', buy)
  // console.log('value :>> ', state.data)

  return (
    <>
      <div className="container-blog">
        <div
          className="image"
          style={{
            backgroundImage: `url("${state.data.image}")`,
            backgroundSize: 'cover',
            backgroundPosition: 'center'
          }}
        >
        </div>
        <div className="details">
            <h1>{state.data.name}</h1>
            <div className="show-icon">
              <div className="updated">
                <span> Color </span> {state.data.color.toString()}
              </div>
              <div className="bottom">
              <span className='bottom-icon'>Price {state.data.price}</span>
                <span className='bottom-icon'>
           Size   {state.data.size}
                </span>
              </div>
            </div>
            <button className='btn btn-outline-success' onClick={(e) => navigate('/buy', { state: buy })}>Buy</button>
          </div>
      </div>

    </>
  )
}

export default Blog
