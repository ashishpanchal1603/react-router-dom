import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import './Furniture.css'
import Navbar from '../pages/Navbar'

function Furniture () {
  const [data, setData] = useState([])
  const getData = () => {
    axios
      .get('https://6364aace7b209ece0f4adfbb.mockapi.io/employee')
      .then((res) => setData(res.data))
  }
  const navigate = useNavigate()
  const handleClick = (e, data) => {
    console.log('value', data)
    navigate('/blog', { state: { data } })
  }
  useEffect(() => {
    getData()
  }, [])
  return (
    <>
      <Navbar />
      {data.map((item, index) => {
        return (
          <div
            className="card"
            key={index}
            onClick={(e) => handleClick(e, item)}
          >
            <div className="img-content">
              <img src={item.image} alt="" />
            </div>
            <div className="card-content">
            <div>
                <h2> {item.name}</h2>
              </div>
              <br />
              <div>
                <h2>{item.description}</h2>
              </div>

              <br />
              <div className="bottom-details">
                <div className="updated">
                  <span> Color </span> {item.color.toString()}
                </div>
              </div>
            </div>
          </div>
        )
      })}
    </>
  )
}

export default Furniture
