import React from 'react'
import './Buy.css'
import { useLocation } from 'react-router-dom'
function Buy () {
  const { state } = useLocation()
  console.log('state.state.data.name', state.state.data.name)
  return (
    <div className='container-buy'>
      <h1> All Details To buy A Item</h1>
      <div className="container-Buy">
        <table>
            <tr>
                <th>item Name <hr /></th>
                <th>item Color<hr /></th>
                <th>item Price <hr /></th>
                <th>item Size <hr /></th>
            </tr>
            <tr>
                <td>{state.state.data.name}</td>
                <td>{state.state.data.color}</td>
                <td>{state.state.data.price}</td>
                <td>{state.state.data.size}</td>
            </tr>
        </table>

      </div>
    </div>
  )
}

export default Buy
